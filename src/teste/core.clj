(ns teste.core
  (:gen-class))

(def lifes 5)


(defn you-loose [] print "you die")
(defn you-win [] print "you win")

(defn hit-all? [word hits]
  (empty? (remove (fn [x] (contains? hits (str x))) word)))

(defn read-letter! [] (read-line))

(defn nice-guess? [guess word] (.contains word (str guess)))

(defn game-situation-printed [word hits]
    (doseq [l (seq word)]
        (if (contains? hits (str l))
            (print l " ") (print "_" " ")))
    (println))


(defn game [lifes-remaining word hits]
  (game-situation-printed word hits)
  (cond 
    (= lifes-remaining 0) (you-loose)
    (hit-all? word hits) (you-win)
    :else
    (let [guess (read-letter!)]
      (if (nice-guess? guess word)
        (do
          (println "You guess it right!")
          (recur lifes-remaining word (conj hits guess)))
        
        (do
          (print "You miss the word!")
          (recur (dec lifes-remaining) word hits))

      )
    )
  )
)


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
)
